<?xml version="1.0" encoding="ISO-8859-1"?>
<nidocument>
<nicomment>
Fehler der von Thomas Bahrmann entwickelten Motorsteuerungen @ Friedrich-Schiller-Universitšt Jena
<nifamily familyname="FSU Motor Error" displayname="FSU Motor Error">
</nifamily>
</nicomment>
<nierror code="505000">
No error
</nierror>
<nierror code="505001">
Switch is not on PC
</nierror>
<nierror code="505002">
Stack is Full
</nierror>
<nierror code="505003">
Incorrect Parameters
</nierror>
<nierror code="505004">
Motor is running
</nierror>
<nierror code="505005">
Incorrect Format
</nierror>
<nierror code="505006">
Limit Switch is triggered
</nierror>
<nierror code="505007">
Incorrect Command
</nierror>
<nierror code="505008">
Motor doesn't exist
</nierror>
<nierror code="505009">
Remote failed
</nierror>
<nierror code="505010">
Data is failure1
</nierror>
<nierror code="505011">
Data is failure2
</nierror>
<nierror code="505012">
Adjust is disabled
</nierror>
<nierror code="505013">
Sofware switch fault
</nierror>
<nierror code="505014">
Mirco-Mode is disabled
</nierror>
<nierror code="505015">
Current error
</nierror>
<nierror code="505016">
load default parameter
</nierror>
<nierror code="505017">
A password is required
</nierror>
<nierror code="505018">
Device not found
</nierror>
<nierror code="505019">
False device
</nierror>
<nierror code="505020">
USB Device not found
</nierror>
<nierror code="505021">
TCP Device not found
</nierror>
<nierror code="505022">
Both endcontacts are triggered
</nierror>
<nierror code="505023">
Timeout Error
</nierror>
</nidocument>
