﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Dependencies" Type="Folder">
			<Item Name="SMS_1603" Type="Folder">
				<Item Name="USB_NET_CHECK_SMS.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/SMS_1603.llb/USB_NET_CHECK_SMS.vi"/>
				<Item Name="USB_NET_CMD_SMS.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/SMS_1603.llb/USB_NET_CMD_SMS.vi"/>
				<Item Name="USB_NET_DAT_SMS.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/SMS_1603.llb/USB_NET_DAT_SMS.vi"/>
				<Item Name="USB_NET_EDAT_SMS.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/SMS_1603.llb/USB_NET_EDAT_SMS.vi"/>
				<Item Name="USB_NET_M_POP.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/SMS_1603.llb/USB_NET_M_POP.vi"/>
				<Item Name="USB_NET_MIC_STRING.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/SMS_1603.llb/USB_NET_MIC_STRING.vi"/>
				<Item Name="USB_NET_RD_INI.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/SMS_1603.llb/USB_NET_RD_INI.vi"/>
				<Item Name="USB_NET_RW_SMS.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/SMS_1603.llb/USB_NET_RW_SMS.vi"/>
				<Item Name="USB_NET_WR_INI.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/SMS_1603.llb/USB_NET_WR_INI.vi"/>
				<Item Name="USB_NET_IO.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/ALL_1603.llb/USB_NET_IO.vi"/>
				<Item Name="USB_MASK.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/ALL_1603.llb/USB_MASK.vi"/>
				<Item Name="USB_NET_ECMD.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/ALL_1603.llb/USB_NET_ECMD.vi"/>
				<Item Name="USB_INIT.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/ALL_1603.llb/USB_INIT.vi"/>
				<Item Name="USB_INFO.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/ALL_1603.llb/USB_INFO.vi"/>
			</Item>
			<Item Name="Error Handling" Type="Folder">
				<Item Name="Create Error File from Enum.vi" Type="VI" URL="../HIJ Motion/Error Handling/Create Error File from Enum.vi"/>
				<Item Name="Error Code 2 Error Cluster.vi" Type="VI" URL="../HIJ Motion/Error Handling/Error Code 2 Error Cluster.vi"/>
				<Item Name="FSU SMS Errors.ctl" Type="VI" URL="../HIJ Motion/Error Handling/FSU SMS Errors.ctl"/>
			</Item>
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="ViewableActor.lvlib" Type="Library" URL="../Viewable Actor/ViewableActor.lvlib"/>
			<Item Name="Named Actor.lvlib" Type="Library" URL="../HIJ Motion/Named Actor/Named Actor.lvlib"/>
			<Item Name="POLARIS Open G.lvlib" Type="Library" URL="../POLARIS OpenG/POLARIS Open G.lvlib"/>
			<Item Name="Reply Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Reply Msg/Reply Msg.lvclass"/>
		</Item>
		<Item Name="Applications" Type="Folder">
			<Item Name="FSU SMS Bahrmann UI.vi" Type="VI" URL="../HIJ Motion/Bahrmann Utility/FSU SMS Bahrmann UI.vi"/>
			<Item Name="HIJ Motion App.lvlib" Type="Library" URL="../HIJ Motion/HIJ Motion App/HIJ Motion App.lvlib"/>
			<Item Name="HIJ Motion General App.lvlib" Type="Library" URL="../HIJ Motion/HIJ Motion General App/HIJ Motion General App.lvlib"/>
		</Item>
		<Item Name="Motor" Type="Folder">
			<Item Name="Motor Actor.lvlib" Type="Library" URL="../HIJ Motion/Motor Actor/Motor Actor.lvlib"/>
			<Item Name="FSU Step Motor.lvlib" Type="Library" URL="../HIJ Motion/FSU Step Motor/FSU Step Motor.lvlib"/>
		</Item>
		<Item Name="Motor Controller" Type="Folder">
			<Item Name="Configurable Motor Controller.lvlib" Type="Library" URL="../HIJ Motion/Configurable Motor Controller/Configurable Motor Controller.lvlib"/>
			<Item Name="Motor Controller Actor.lvlib" Type="Library" URL="../HIJ Motion/Motor Contoller Actor/Motor Controller Actor.lvlib"/>
			<Item Name="FSU SMS Actor.lvlib" Type="Library" URL="../HIJ Motion/FSU SMS Actor/FSU SMS Actor.lvlib"/>
		</Item>
		<Item Name="UI" Type="Folder">
			<Item Name="Dialogs" Type="Folder">
				<Item Name="Calibration Config Dialog.lvlib" Type="Library" URL="../HIJ Motion/Calibration Config Dialog/Calibration Config Dialog.lvlib"/>
				<Item Name="Special Positions Dialog.lvlib" Type="Library" URL="../HIJ Motion/Special Positions Dialog/Special Positions Dialog.lvlib"/>
			</Item>
			<Item Name="Motor UI Base.lvlib" Type="Library" URL="../HIJ Motion/Motor UI Base/Motor UI Base.lvlib"/>
			<Item Name="Angle Motor UI.lvlib" Type="Library" URL="../HIJ Motion/Angle Motor UI/Angle Motor UI.lvlib"/>
			<Item Name="FilterWheelSets.lvlib" Type="Library" URL="../HIJ Motion/FilterWheelSets/FilterWheelSets.lvlib"/>
			<Item Name="FilterSet UI.lvlib" Type="Library" URL="../HIJ Motion/FilterSet UI/FilterSet UI.lvlib"/>
			<Item Name="Filter Slide.lvlib" Type="Library" URL="../HIJ Motion/Filter Slide/Filter Slide.lvlib"/>
			<Item Name="FSU SMS Device UI.lvlib" Type="Library" URL="../HIJ Motion/FSU SMS Device UI/FSU SMS Device UI.lvlib"/>
			<Item Name="EndContact Motor UI.lvlib" Type="Library" URL="../HIJ Motion/EndContact Motor UI/EndContact Motor UI.lvlib"/>
			<Item Name="Simple Motor GUI.lvlib" Type="Library" URL="../HIJ Motion/Simple Motor GUI/Simple Motor GUI.lvlib"/>
			<Item Name="Simple Motor Abs Pos UI.lvlib" Type="Library" URL="../HIJ Motion/Simple Motor Abs Pos UI/Simple Motor Abs Pos UI.lvlib"/>
			<Item Name="Slide Motor UI.lvlib" Type="Library" URL="../HIJ Motion/Slide Motor UI/Slide Motor UI.lvlib"/>
			<Item Name="Special Position UI Base.lvlib" Type="Library" URL="../HIJ Motion/Special Position UI Base/Special Position UI Base.lvlib"/>
			<Item Name="Special Positions Motor UI.lvlib" Type="Library" URL="../HIJ Motion/Special Positions Motor UI/Special Positions Motor UI.lvlib"/>
		</Item>
		<Item Name="HIJ Motion App.ico" Type="Document" URL="../HIJ Motion/HIJ Motion App/HIJ Motion App/HIJ Motion App.ico"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="LVFontTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVFontTypeDef.ctl"/>
				<Item Name="LVStringsAndValuesArrayTypeDef_U16.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVStringsAndValuesArrayTypeDef_U16.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
			</Item>
			<Item Name="FTD2XX.dll" Type="Document" URL="FTD2XX.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="FSU SMS Device" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{05D3775B-55A2-42E8-A8CD-E5E009E89C0E}</Property>
				<Property Name="App_INI_GUID" Type="Str">{4ED8DAF2-C999-4DC5-B90D-198F28A0B457}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Microsoft.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{720FCDAA-1407-4217-9DE7-EC3503697E58}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Kontrol Aplikation für Bahrmann FSU SMS.</Property>
				<Property Name="Bld_buildSpecName" Type="Str">FSU SMS Device</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/FSU SMS Device</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{15CD0720-B1D5-414F-ACF4-FA2D78D54E36}</Property>
				<Property Name="Bld_version.build" Type="Int">21</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">FSU SMS Device.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/FSU SMS Device/FSU SMS Device.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/FSU SMS Device/data</Property>
				<Property Name="Destination[2].destName" Type="Str">Config</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/NI_AB_PROJECTNAME/FSU SMS Device/Config</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Exe_iconItemID" Type="Ref"></Property>
				<Property Name="Source[0].itemID" Type="Str">{9AB0AE0A-CA88-42B9-A985-9E12193992F9}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].lvfile" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Applications/FSU SMS Bahrmann UI.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/UI/FSU SMS Device UI.lvlib/Launch SMS and UI.vi</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[4].itemID" Type="Ref"></Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref"></Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">6</Property>
				<Property Name="TgtF_companyName" Type="Str">Microsoft</Property>
				<Property Name="TgtF_fileDescription" Type="Str">FSU SMS Device</Property>
				<Property Name="TgtF_internalName" Type="Str">FSU SMS Device</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 Microsoft</Property>
				<Property Name="TgtF_productName" Type="Str">FSU SMS Device</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{7C6751C1-2973-4E57-B088-610605F7A250}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">FSU SMS Device.exe</Property>
			</Item>
			<Item Name="Simple Motor UI" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{EC460BCB-888F-4D9B-AF47-F01F96D54661}</Property>
				<Property Name="App_INI_GUID" Type="Str">{078F9993-4CF3-404B-99CD-26C749398116}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Microsoft.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{9CDE4A8C-F4D1-49FE-A7B8-B44FDCD679B6}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Simple Motor UI</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Simple Motor UI</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{14C7BFFB-711B-4037-94CD-DBCC12628402}</Property>
				<Property Name="Bld_version.build" Type="Int">6</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Simple Motor UI.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Simple Motor UI/Simple Motor UI.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Simple Motor UI/data</Property>
				<Property Name="Destination[2].destName" Type="Str">Config</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/NI_AB_PROJECTNAME/Simple Motor UI/Config</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Exe_iconItemID" Type="Ref"></Property>
				<Property Name="Source[0].itemID" Type="Str">{EDAC05C5-5B17-402D-9D8A-7F505C06D6DC}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].lvfile" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Applications/FSU SMS Bahrmann UI.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/UI/FSU SMS Device UI.lvlib/Launch SMS and UI.vi</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[4].itemID" Type="Ref"></Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/UI/Simple Motor GUI.lvlib/Launch Simple Motor UI.vi</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">6</Property>
				<Property Name="TgtF_companyName" Type="Str">Microsoft</Property>
				<Property Name="TgtF_fileDescription" Type="Str">FSU SMS Device</Property>
				<Property Name="TgtF_internalName" Type="Str">FSU SMS Device</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 Microsoft</Property>
				<Property Name="TgtF_productName" Type="Str">FSU SMS Device</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{6D466ADE-B494-4FF2-B8FE-F9D6F7067E22}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Simple Motor UI.exe</Property>
			</Item>
			<Item Name="HIJ General Motion App" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{03E87BF3-05DE-4D58-8C41-32F786A44A71}</Property>
				<Property Name="App_INI_GUID" Type="Str">{7AB676B2-212F-42E4-A6DD-F5CF54EB67EF}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.Microsoft.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{E9DA3FE5-2485-4CFE-BD1D-CCE00F24EB63}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">HIJ General Motion App</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/HIJ General Motion App</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{5AEBC2EE-8236-4865-8502-9F06CD92F395}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.patch" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">HIJ General Motion App.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/HIJ General Motion App/HIJ General Motion App.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/HIJ General Motion App/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[2].destName" Type="Str">Config</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/HIJ General Motion App/Config</Property>
				<Property Name="Destination[2].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/HIJ Motion App.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{329A318D-A7BE-47A3-AE32-05758C1DD227}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Applications/FSU SMS Bahrmann UI.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/UI/FSU SMS Device UI.lvlib/Launch SMS and UI.vi</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/UI/Simple Motor GUI.lvlib/Launch Simple Motor UI.vi</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Applications/HIJ Motion General App.lvlib/Launcher.vi</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Motor Controller/FSU SMS Actor.lvlib/Errors/FSU Motor Controller-errors.txt</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">6</Property>
				<Property Name="TgtF_companyName" Type="Str">FSU-Jena</Property>
				<Property Name="TgtF_fileDescription" Type="Str">HIJ General Motion App</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 FSU-Jena</Property>
				<Property Name="TgtF_productName" Type="Str">HIJ General Motion App</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{B72F8EFB-7B80-4A22-A839-6357DDC7C43B}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">HIJ General Motion App.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
